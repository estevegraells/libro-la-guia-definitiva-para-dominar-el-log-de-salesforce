#!/bin/bash

#Obtenemos los IDs de los logs generados por el usuario y quedan almacenados en un fichero CSV
echo $(sfdx force:data:soql:query -u DevHub -q "SELECT Id FROM ApexLog WHERE LogUserId='00509000000e4K6AAI' AND StartTime>2020-09-29T08:00:00.000+0000" -r "csv" > logs_a_borrar.csv )

# Ejecutamos un borrado masivo pasando como fichero de referencia
echo $(sfdx force:data:bulk:delete -u DevHub -s ApexLog -f logs_a_borrar.csv)

NUM_LOGS_USUARIO=$(sfdx force:data:soql:query -u DevHub -q  "SELECT count(Id) FROM ApexLog WHERE LogUserId='00509000000e4K6AAI' AND StartTime>2020-09-29T08:00:00.000+0000")
echo "El número de logs en el servidor son: " $NUM_LOGS_USUARIO