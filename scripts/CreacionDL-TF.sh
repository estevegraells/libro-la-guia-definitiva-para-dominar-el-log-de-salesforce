#
# @author Esteve Graells
# @date 2020
#
# @description Creacion de un TraceFlag con un DebugLevel para un usuario
#

#!/bin/bash

####################################
#Obtener el Id del usuario
while true; do
    read -p "Quieres introducir el ID del usuario o su nombre y apellido (ID/NA)?" respuesta
    case $respuesta in
        [ID]* ) read -p "ID?" USUARIO_ID; break;;
        [NA]* ) read -p "Nombre?" NOMBRE_US;
                read -p "Apellido?" APELLIDO_US;
                USUARIO_ID=$(sfdx force:data:soql:query -u DevHub -q "SELECT Id FROM User WHERE FirstName='$NOMBRE_US' AND LastName='$APELLIDO_US'" --json | jq '.result .records[] .Id');
                break;;

        * ) echo "Debería ser ID ó N";;
    esac
done

echo "EL ID del usuario es: " $USUARIO_ID


####################################
#Creación del Debug Level
read -p "Cual es el alias de la Org? " ALIAS_ORG
read -p "Cual es el nombre del Debug Level a crear? " NOMBRE_DL
read -p "Cual es el nivel de debug para la categoria DATABASE [NONE|INFO|FINEST]?" CAT_DB_LEVEL
read -p "Cual es el nivel de debug para la categoria WORKFLOW [NONE|ERROR|WARN|INFO|FINE|FINER]?" CAT_WF_LEVEL
read -p "Cual es el nivel de debug para la categoria VALIDATION [NONE|INFO]?" CAT_VL_LEVEL
read -p "Cual es el nivel de debug para la categoria CALLOUT [NONE|ERROR|INFO|FINE|FINER]?" CAT_CO_LEVEL
read -p "Cual es el nivel de debug para la categoria APEXCODE [NONE|ERROR|WARN|INFO|FINE|FINER]?" CAT_AC_LEVEL
read -p "Cual es el nivel de debug para la categoria APEXPROFILING [NONE|ERROR|WARN|INFO|DEBUG|FINE|FINER|FINEST]?" CAT_AP_LEVEL
read -p "Cual es el nivel de debug para la categoria VISUALFORCE [NONE|ERROR|WARN|INFO|DEBUG|FINE|FINER|FINEST]?" CAT_VF_LEVEL
read -p "Cual es el nivel de debug para la categoria SYSTEM [NONE|ERROR|WARN|INFO|DEBUG|FINE|FINER|FINEST]?" CAT_SY_LEVEL
read -p "Cual es el nivel de debug para la categoria WAVE [NONE|ERROR|WARN|INFO|DEBUG|FINE|FINER|FINEST]?" CAT_WV_LEVEL
read -p "Cual es el nivel de debug para la categoria NBA [NONE|ERROR|WARN|INFO|DEBUG|FINE|FINER|FINEST]?" CAT_NB_LEVEL

echo $(sfdx force:data:record:create -u $ALIAS_ORG -s DebugLevel -t -v "DEVELOPERNAME=$NOMBRE_DL MASTERLABEL=$NOMBRE_DL DATABASE=$CAT_DB_LEVEL WORKFLOW=$CAT_WF_LEVEL VALIDATION=$CAT_VL_LEVEL CALLOUT=$CAT_CO_LEVEL APEXCODE=$CAT_AC_LEVEL APEXPROFILING=$CAT_AP_LEVEL VISUALFORCE=$CAT_VF_LEVEL SYSTEM=$CAT_SY_LEVEL WAVE=$CAT_WV_LEVEL NBA=$CAT_NB_LEVEL")

#Obtengo el Identificador del Debug Level Creado
DEBUG_LEVEL_ID=$(sfdx force:data:record:get -u DevHub -t -s DebugLevel -w "MASTERLABEL=$NOMBRE_DL" --json | jq '.result .Id')

####################################
#Creación del TraceFlag
read -p "A qué hora debe empezar el Log? [aaaa-mm-ddThh:mm:ss.000+0000]? " INICIO_LOG
read -p "A qué hora debe acabar el Log (máximo 24h)? [aaaa-mm-ddThh:mm:ss.000+0000]? " FIN_LOG

TRACE_FLAG_ID=$(sfdx force:data:record:create -u DevHub -s TraceFlag -t -v "TracedEntityId=$USUARIO_ID DebugLevelId=$DEBUG_LEVEL_ID StartDate=$INICIO_LOG ExpirationDate=$FIN_LOG LogType=USER_DEBUG")



