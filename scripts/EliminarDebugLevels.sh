#
# @author Esteve Graells
# @date 2020
#
# @description Eliminar Debug Levels cuyo ApexCode=INFO
#

#!/bin/bash

# Obtener todos los debug levels cuyo categoría ApexCode tenga el level INFO
DEBUG_LEVELS_IDS=$(sfdx force:data:soql:query -u DevHub -t -q "SELECT Id FROM DebugLevel WHERE ApexCode='INFO'" -r "json" | jq '.result .records[] .Id' -r)

# Eliminar todos los Debug Levels obtenidos
for debug_level_id in $DEBUG_LEVELS_IDS
do
    echo $(sfdx force:data:record:delete -u DevHub -t -s DebugLevel -i $debug_level_id)
done