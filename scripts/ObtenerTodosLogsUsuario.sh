#!/bin/bash
# Obtener todos los logs de un usuario, guardarlos y opcionalmente generar un fichero con el log completo

#Obtenemos los IDs de los logs generados por el usuario
LOG_IDS=$(sfdx force:data:soql:query -u DevHub -t -q "SELECT Id FROM ApexLog WHERE LogUserId='00509000000e4K6AAI' AND StartTime>2020-09-29T08:00:00.000+0000 ORDER BY StartTime" --json | jq '.result .records[] .Id' -r)

i=1

for log_id in $LOG_IDS
do
    #log_id="${log_id//\"}" #Eliminamos las comillas

    echo $(sfdx force:apex:log:get -u DevHub -i $log_id > $i-$log_id.json) #Guardamos el log descargado en un fichero con numeración secuencial
    echo "Log con ID: " $log_id.json " descargado y renombrado a " $i-$log_id.json

    cat  $i-$log_id.json >>  log_completo.json # Opcionalmente generamos un fichero único de log

    ((i=i+1))
done