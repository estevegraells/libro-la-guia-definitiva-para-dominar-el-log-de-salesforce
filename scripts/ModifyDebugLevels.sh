#
# @author Esteve Graells
# @date 2020
#
# @description Modifica toods los DebugLevel para que la categoría ApexCode tenga el debug level Finer
#

#!/bin/bash

#Obtener todos los Debug Level existentes
DEBUG_LEVELS_IDS=$(sfdx force:data:soql:query -u DevHub -t -q "SELECT Id FROM DebugLevel WHERE ApexCode='FINER'" -r "json" | jq '.result .records[] .Id' -r)

#Para cada DebugLevel modificar el valor del level de la categoría ApexCode
for debug_level_id in $DEBUG_LEVELS_IDS
do
    echo $(sfdx force:data:record:update -u DevHub -t -s DebugLevel -i $debug_level_id -v "APEXCODE=FINER")
done